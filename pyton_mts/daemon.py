import schedule
import time
import configparser
import subprocess

config = configparser.ConfigParser()
config.read("settings.ini")
sendtime = config['confluence']['sending_time']

print(sendtime)


def job():
    subprocess.Popen(['python3', 'export.py'])
schedule.every().day.at(sendtime).do(job)


while True:
    schedule.run_pending()
    time.sleep(1)
