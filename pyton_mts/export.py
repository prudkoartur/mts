import configparser
import pandas
from atlassian import Confluence
from datetime import datetime as DT, timedelta
import smtplib
from email.mime.text import MIMEText
from email.header import Header
import numpy as np
import pandas as pd
import pdftables_api


config = configparser.ConfigParser()
config.read("settings.ini")

url_site = config['confluence']['confluence_url']
username_site = config['confluence']['confluence_user']
password_site = config['confluence']['confluence_password']
smtpserver = config['confluence']['confluence_imap']
smtpserver_user = config['confluence']['confluence_imap_user']
smtpserver_password = config['confluence']['confluence_imap_password']
confluence = Confluence(
    url=url_site,
    username=username_site,
    password=password_site
    )

page = confluence.get_page_by_id(page_id=65614)
your_fname = "abc.pdf"


def save_file(content):
    file_pdf = open(your_fname, 'wb')
    file_pdf.write(content)
    file_pdf.close()

response = confluence.get_page_as_pdf(page['id'])
save_file(content=response)


c = pdftables_api.Client('g2vem6anzrg9')
c.xlsx('abc.pdf', 'abc.xlsx')

#  оставляем нужные колонки и переформатируем их названия.
pdex = pd.read_excel('abc.xlsx', header=[1])

pdex.rename(
    columns={
      'No': 'NUM', 'ФИ': 'FIO',
      'Направление работы': 'DOL', 'ФИО': 'FUL',
      'e-mail': 'MAIL', 'IP': 'IP',
      'Мобильный': 'MOB', 'День\nрождения': 'DRT',
      'Login\nAD': 'LOGIN', 'Комна\nта': 'RUM',
      'Табельный\nномер': 'TAB', }, inplace=True)
pdex = pdex.replace(['\n'], '', regex=True)
del pdex['NUM']
del pdex['FIO']
del pdex['IP']
del pdex['LOGIN']
del pdex['TAB']
del pdex['MOB']
del pdex['DOL']
pdex = pdex[pdex.RUM != "уволен"]
pdex.to_excel("abc_clean.xlsx")


filename = r"abc_clean.xlsx"
dob_col = "DRT"
#  читаем ДР как строки, а не как вещественные числа
#  у кого ДР и мы их исключаем из рассылки
dfadresat = pd.read_excel(filename, header=0, dtype={dob_col: str})
days = 8
today = DT.today().date()
dt_from = today - timedelta(days=days)
adresat = (dfadresat
           .assign(dt=pd.to_datetime(
                   dfadresat[dob_col] + ".{}".format(today.year),
                   dayfirst=True))
           .query("(@dt_from <= dt <= @today)"))
#  for i in range(0, len(adresat)):
#      print (adresat.iloc[i]['MAIL'])


filename = r"abc_clean.xlsx"
dob_col = "DRT"
# читаем ДР как строки, а не как вещественные числа кому надо делать рассылку
df = pd.read_excel(filename, header=0, dtype={dob_col: str})
days = 8
today = DT.today().date()
dt_from = today - timedelta(days=days)

res = (df
       .assign(dt=pd.to_datetime(
               df[dob_col] + ".{}".format(today.year), dayfirst=True))
       .query("not (@dt_from <= dt <= @today)")
       )

#  Создаем функцию, отвечающую за отправку письма


def send_mail(addr, body):
    mailsender = smtplib.SMTP(smtpserver, 587)
    mailsender.starttls()
    mailsender.login(smtpserver_user, smtpserver_password)
    mail_recipient = addr
    mail_subject = "Наши именинники"
    mail_body = mailbody
    msg = MIMEText(mail_body, 'plain', 'utf-8')
    msg['Subject'] = Header(mail_subject, 'utf-8')
    mailsender.sendmail('adm@evrosan.net', mail_recipient, msg.as_string())
    mailsender.quit()
    print('Сообщение на адрес', mail_recipient, 'отправлено')

#  \n перевод на новую строку в сообщение , join объеденяет str из цикла.
mailbody = "\n".join(
    ["Наш коллега: {}\t скоро празднует День Рождение, которое состоится: {}"
     .format(x.FUL, x.dt)
     for x in adresat.itertuples()])
#  В цикле перебираем почтовые сообщения
#  кому отправлять и подкрепляем тело сообщение
for addr in res["MAIL"].to_list():
    send_mail(addr, mailbody)

#  Код ниже для того что бы по каждому имениннику было отдельное письмо.
#  for x in adresat.itertuples():
#  mailbody = "Наш коллега: {}\t: скоро празднует
#  День Рождение, которое состоится: {}".format(x.FUL, x.dt)
#  for addr in res["MAIL"].to_list():
#      send_mail(addr, mailbody)
